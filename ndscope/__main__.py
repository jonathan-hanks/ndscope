from __future__ import division, unicode_literals
import os
import sys
try:
    import yaml
except ImportError:
    pass
import argparse
from gpstime import gpstime, GPSTimeException
try:
    from qtpy.QtWidgets import QApplication, QStyleFactory
except ImportError:
    from PyQt5.QtWidgets import QApplication, QStyleFactory
import signal
try:
    from setproctitle import setproctitle
except ImportError:
    def setproctitle(*args):
        pass
import logging

from . import __version__
from . import const
from . import util
from . import layout
from .scope import set_background, NDScope

logging.addLevelName(5, 'DATA')
logging.basicConfig(
    level=os.getenv('LOG_LEVEL', 'WARNING'),
    format="%(threadName)s:%(message)s",
)

##################################################

DEFAULT_TIME_WINDOW_ONLINE = [-2, 1/16]
DEFAULT_TIME_WINDOW_OFFLINE = [-10, 10]


PROG = 'ndscope'
USAGE = '''
ndscope [<options>]
ndscope [<options>] channel ...
ndscope [<options>] .yaml|.stp|.txt|-
ndscope -h|--help|--usage|--version
'''
DESCRIPTION = 'Next generation NDS oscilloscope'
FULL_DESCRIPTION = '''

If no time is specified, online data will be plotted.  The -t option
may be used to specify a time in the past.  Multiple -t options
specify a time range.  Times can be expressed in GPS, Gregorian, or
relative (e.g.  "3 days ago").  Remember to use quotes around
command-line time specifications that include spaces.
Example:

  ndscope H1:GRD-ISC_LOCK_STATE_N
  ndscope -t '2 days ago' -t '1 day ago' H1:GRD-ISC_LOCK_STATE_N

The -w option allows specifying an initial window around a single -t
specification, or an initial lookback time window for online mode.
Windows can be either a single number specifying a full window width,
or a comma-separated pair specifying times relative to the specified
center time.  The ordering of numbers in the winodw does not matter,
as they will be ordered automatically.  If only one number is
specified with a comma the other is assumed to be zero.  Brackets '[]'
can be used around windows to circumvent CLI parsing confusion.  The
following windows are equivalent: '[-10,0]', '[-10,]', ',-10', '0,-10'
Example:

  ndscope -t 1224990999 -w 1,-10 H1:SUS-PRM_M3_MASTER_OUT_UL_DQ

Left and right mouse buttons control pan and zoom respectively, as
does center wheel.  Missing data will automatically be fetched as
needed when panning and zooming.  In offline mode, second and minute
trend data are substituted automatically depending on the time scale
being requested/displayed.

WARNING: Online mode uses only raw, full-rate data, so large windows
can adversely affect performance when working with fast channels.

By default all channels are placed into a grid (row-major ordering).
If the --table/-l option is provided, periods and commas in the
channel list (space separated) will cause new subplots to be created,
with periods starting new rows of plots.
Example:

  ndscope H1:FOO , H1:BAR H1:BAZ . H1:QUX

causes three subplots to be created, two in the top row (the second
with two channels H1:BAR and H1:BAZ), and one in the second.  If the
--table option is not provided, periods and commas in the channel list
are ignored.

Plot templates can be loaded from ndscope .yaml, StripTool .stp, or
.txt template files (stdin assumes yaml).  An ndscope .yaml config
template can be generated with the --gen-template option.  The
"plots:" block in the configuration is a list of subplot definitions.
Each subplot should include a "channels" mapping of channel names to
curve properties, such as "color", "width", "scale", and "offset".
Example:

  ndscope H1:FOO H1:BAR H1:BAZ --stack --gen-template > my_template.yaml
  ndscope my_template.yaml

NDS server[:port] should be provided in the NDSSERVER environment
variable.
Example:

  NDSSERVER=nds.ligo-la.caltech.edu ndscope L1:FOO

Please report issues: https://git.ligo.org/cds/ndscope/issues

Environment variables:
  NDSSERVER    HOST[:PORT] of desired NDS server
  DATETIME_TZ  Timezone: 'UTC' (default) or 'LOCAL'
  LOG_LEVEL    Turn on logging ('INFO', 'DEBUG', etc.)
  ANTIALIAS    Turn on anti-aliasing (possible performance hit)
'''


class TimeParseAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=False):
        try:
            gps = gpstime.parse(values).gps()
        except GPSTimeException:
            parser.error("Could not parse time string '{}'".format(values))
        if getattr(namespace, self.dest) is None:
            setattr(namespace, self.dest, [gps])
        else:
            getattr(namespace, self.dest).append(gps)


parser = argparse.ArgumentParser(
    prog=PROG,
    usage=USAGE,
    description=DESCRIPTION,
    formatter_class=argparse.RawDescriptionHelpFormatter,
    add_help=False,
)

parser.add_argument('-h', '--help', action='help',
                    help=argparse.SUPPRESS)
parser.add_argument('--usage', action='store_true',
                    help=argparse.SUPPRESS)
parser.add_argument('channels', nargs='*',
                    help=argparse.SUPPRESS)
parser.add_argument('-t', '--time', action=TimeParseAction, default=[],
                    help="time boundary (GPS or natural language), may specify one or two)")
parser.add_argument('-w', '--window', '--time-window', dest='time_window',
                    help="time window scalar or tuple, in seconds")
lgroup = parser.add_mutually_exclusive_group()
lgroup.add_argument('-g', '--grid', dest='layout', action='store_const', const='grid',
                    help="arrange channels in a grid of plots (default)")
lgroup.add_argument('-k', '--stack', dest='layout', action='store_const', const='stack',
                    help="arrange channels in a vertical stack of plots")
lgroup.add_argument('-s', '--single', dest='layout', action='store_const', const='single',
                    help="all channels in a single plot")
lgroup.add_argument('-l', '--table', dest='layout', action='store_const', const='table',
                    help="subplot table layout (period/comman in channel list starts new colum/row)")
# parser.add_argument('--colspan', action='store_true',
#                     help="expand subplots to fill empty columns (BUGGY!)")
parser.add_argument('--title', '--window-title', dest='window_title', metavar='TITLE',
                    help="application window title")
parser.add_argument('-bw', '--black-on-white', action='store_true',
                    help="black-on-white plots, instead of white-on-black")
parser.add_argument('--gen-template', action='store_true',
                    help="generate YAML layout, dump to stdout, and exit")
parser.add_argument('--version', action='version', version='%(prog)s {}'.format(__version__),
                    help="print version and exit")


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    setproctitle(' '.join([PROG] + sys.argv[1:]))

    args = parser.parse_args()

    if args.usage:
        parser.exit(message=DESCRIPTION+FULL_DESCRIPTION)

    ##########
    # load template

    if args.channels and (os.path.exists(args.channels[0]) or args.channels[0] == '-'):
        if len(args.channels) > 1:
            parser.error("Only one argument expected when specifying template file.")
        template, ltype = layout.load_template(args.channels[0])
    else:
        template, ltype = layout.template_from_chans(args.channels)
        if not args.channels:
            ltype = args.layout
        elif ',' in args.channels or '.' in args.channels:
            args.layout = 'table'
        elif not args.layout:
            args.layout = 'grid'

    ##########
    # command line argument overlay

    if args.layout and args.layout != ltype:
        layout.convert_layout(template, args.layout)

    if args.black_on_white:
        template['black-on-white'] = args.black_on_white

    if args.window_title:
        template['window-title'] = args.window_title

    if args.time_window:
        template['time-window'] = args.time_window
    try:
        template['time-window'] = util.parse_time_window(template.get('time-window'))
    except:
        parser.error("Could not parse time window: {}".format(template.get('time-window')))

    ##########
    # parse time specs

    if len(args.time) == 0:
        online = True
        t0 = None
        if template['time-window']:
            w = template['time-window']
            template['time-window'][0] -= w[1]
            template['time-window'][1] = DEFAULT_TIME_WINDOW_ONLINE[1]
        else:
            template['time-window'] = list(DEFAULT_TIME_WINDOW_ONLINE)
    elif len(args.time) == 1:
        online = False
        t0 = args.time[0]
        if not template['time-window']:
            template['time-window'] = list(DEFAULT_TIME_WINDOW_OFFLINE)
    elif len(args.time) == 2:
        online = False
        template['time-window'] = [0, abs(args.time[0] - args.time[1])]
        t0 = min(args.time)
    else:
        parser.error("May only specify one or two times.")

    ##########
    # launch app

    if args.gen_template:
        template['version'] = 'ndscope {}'.format(__version__)
        print(yaml.safe_dump(template, default_style=False))
        sys.exit()

    if template.get('black-on-white'):
        set_background('w')

    if not const.HOST:
        sys.exit("NDSSERVER environment variable not specified.")

    app = QApplication([])
    app.setStyle(QStyleFactory.create("Plastique"))
    app.setStyleSheet("QPushButton { background-color: #CCC }")
    scope = NDScope(template['plots'])
    scope.setWindowTitle('{}: {}'.format(PROG, template.get('window-title', '')))
    scope.show()

    if not template['plots'][0]['channels']:
        pass
    elif online:
        scope.start(template['time-window'])
    else:
        scope.fetch(t0=t0, window=template['time-window'])

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
