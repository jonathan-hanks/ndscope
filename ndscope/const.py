import os
import re
from dateutil.tz import tzutc, tzlocal


try:
    HOSTPORT = os.getenv('NDSSERVER').split(',')[0].split(':')
except AttributeError:
    HOSTPORT = [None]
HOST = HOSTPORT[0]
try:
    PORT = int(HOSTPORT[1])
except IndexError:
    PORT = 31200


# date/time formatting for GPS conversion
if os.getenv('DATETIME_TZ') == 'LOCAL':
    DATETIME_TZ = tzlocal()
else:
    DATETIME_TZ = tzutc()
# FIXME: why does '%c' without explicit TZ give very wrong values??
#DATETIME_FMT = '%c'
DATETIME_FMT = '%a %b %d %Y %H:%M:%S %Z'
DATETIME_FMT_OFFLINE = '%Y/%m/%d %H:%M:%S %Z'


# thresholds in seconds where requests translate to second and minute
# trends
TREND_THRESHOLD_S = 120
TREND_THRESHOLD_M = 3600


# transformations for the time axis.  will transform when span is x10
# larger than the scale factor.  should be ordered largest to
# smallest
TIME_AXIS_TRANSFORMS = [
    (1, 'seconds'),
    (60, 'minutes'),
    (3600, 'hours'),
    (86400, 'days'),
    (604800, 'weeks'),
    (2592000, 'months'),
    (31536000, 'years'),
]


# percentage of full span to add as additional padding when fetching
# new data
DATA_SPAN_PADDING = 0.5


# number of lookback bytes available per channel
# 2**22:             4194304
# one week of 16 Hz: 4838400
# 60s of 16k Hz:     7864320
# 2**23:             8388608
DATA_LOOKBACK_LIMIT_BYTES = 2**22


# regular expression to match channel strings
CHANNEL_REGEXP = '^([a-zA-Z0-9-]+:)?[a-zA-Z0-9-_\.]+$'
CHANNEL_RE = re.compile(CHANNEL_REGEXP)


# timeout for NDS error messages in the status bar
STATUS_ERROR_TIMEOUT = 5000
